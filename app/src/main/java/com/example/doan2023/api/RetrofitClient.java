package com.example.doan2023.api;

import com.example.doan2023.Constant.ApiConstant;
import com.example.doan2023.model.Word;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    private Retrofit retrofit;
    public Retrofit getRetrofitInstance(String baseURL) {
        retrofit = new Retrofit.Builder()
                .baseUrl(baseURL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit;
    }
}
