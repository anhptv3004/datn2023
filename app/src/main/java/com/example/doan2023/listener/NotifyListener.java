package com.example.doan2023.listener;

public interface NotifyListener {
    void onUpdateFromNotify();
    void onStopNotify();
}
