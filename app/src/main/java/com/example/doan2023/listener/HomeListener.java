package com.example.doan2023.listener;

import com.example.doan2023.model.Word;

public interface HomeListener {
    void onUpdateFromHome(Word word);
    void onUpdateFromSearch(String text);
}
