package com.example.doan2023.listener;

public interface AdapterListener {
    void onUpdateFromAdapter(String word);
}
